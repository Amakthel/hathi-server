package activitypub

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	"net/url"
	"strings"
	"time"

	"github.com/peterhellberg/duration"
)

// Maps the built-in field terms to their IRIs
var BuiltInFields = map[string]string{
	"@id":          "@id",
	"id":           "@id",
	"@type":        "@type",
	"type":         "@type",
	"actor":        "www.w3.org/ns/activitystreams#actor",
	"attachment":   "www.w3.org/ns/activitystreams#attachment",
	"attributedTo": "www.w3.org/ns/activitystreams#attributedTo",
	"audience":     "www.w3.org/ns/activitystreams#audience",
	"bcc":          "www.w3.org/ns/activitystreams#bcc",
	"bto":          "www.w3.org/ns/activitystreams#bto",
	"cc":           "www.w3.org/ns/activitystreams#cc",
	"context":      "www.w3.org/ns/activitystreams#context",
	"current":      "www.w3.org/ns/activitystreams#current",
	"first":        "www.w3.org/ns/activitystreams#first",
	"generator":    "www.w3.org/ns/activitystreams#generator",
	"icon":         "www.w3.org/ns/activitystreams#icon",
	"image":        "www.w3.org/ns/activitystreams#image",
	"inReplyTo":    "www.w3.org/ns/activitystreams#inReplyTo",
	"instrument":   "www.w3.org/ns/activitystreams#instrument",
	"last":         "www.w3.org/ns/activitystreams#last",
	"location":     "www.w3.org/ns/activitystreams#location",
	"items":        "www.w3.org/ns/activitystreams#items",
	"oneOf":        "www.w3.org/ns/activitystreams#oneOf",
	"anyOf":        "www.w3.org/ns/activitystreams#anyOf",
	"closed":       "www.w3.org/ns/activitystreams#closed",
	"origin":       "www.w3.org/ns/activitystreams#origin",
	"next":         "www.w3.org/ns/activitystreams#next",
	"object":       "www.w3.org/ns/activitystreams#object",
	"prev":         "www.w3.org/ns/activitystreams#prev",
	"preview":      "www.w3.org/ns/activitystreams#preview",
	"result":       "www.w3.org/ns/activitystreams#result",
	"replies":      "www.w3.org/ns/activitystreams#replies",
	"tag":          "www.w3.org/ns/activitystreams#tag",
	"target":       "www.w3.org/ns/activitystreams#target",
	"to":           "www.w3.org/ns/activitystreams#to",
	"url":          "www.w3.org/ns/activitystreams#url",
	"accuracy":     "www.w3.org/ns/activitystreams#accuracy",
	"altitude":     "www.w3.org/ns/activitystreams#altitude",
	"content":      "www.w3.org/ns/activitystreams#content",
	"name":         "www.w3.org/ns/activitystreams#name",
	"duration":     "www.w3.org/ns/activitystreams#duration",
	"height":       "www.w3.org/ns/activitystreams#height",
	"href":         "www.w3.org/ns/activitystreams#href",
	"hreflang":     "www.w3.org/ns/activitystreams#hreflang",
	"partOf":       "www.w3.org/ns/activitystreams#partOf",
	"latitude":     "www.w3.org/ns/activitystreams#latitude",
	"longitude":    "www.w3.org/ns/activitystreams#longitude",
	"mediaType":    "www.w3.org/ns/activitystreams#mediaType",
	"endTime":      "www.w3.org/ns/activitystreams#endTime",
	"published":    "www.w3.org/ns/activitystreams#published",
	"startTime":    "www.w3.org/ns/activitystreams#startTime",
	"radius":       "www.w3.org/ns/activitystreams#radius",
	"rel":          "www.w3.org/ns/activitystreams#rel",
	"startIndex":   "www.w3.org/ns/activitystreams#startIndex",
	"summary":      "www.w3.org/ns/activitystreams#summary",
	"totalItems":   "www.w3.org/ns/activitystreams#totalItems",
	"units":        "www.w3.org/ns/activitystreams#units",
	"updated":      "www.w3.org/ns/activitystreams#updated",
	"width":        "www.w3.org/ns/activitystreams#width",
	"subject":      "www.w3.org/ns/activitystreams#subject",
	"relationship": "www.w3.org/ns/activitystreams#relationship",
	"describes":    "www.w3.org/ns/activitystreams#describes",
	"formerType":   "www.w3.org/ns/activitystreams#formerType",
	"deleted":      "www.w3.org/ns/activitystreams#deleted",
}

// Maps the built-in type terms to their IRIs
var BuiltInObjectTypes = map[string]string{
	"Object":                "www.w3.org/ns/activitystreams#Object",
	"Link":                  "www.w3.org/ns/activitystreams#Link",
	"Activity":              "www.w3.org/ns/activitystreams#Activity",
	"IntransitiveActivity":  "www.w3.org/ns/activitystreams#IntransitiveActivity",
	"Collection":            "www.w3.org/ns/activitystreams#Collection",
	"OrderedCollection":     "www.w3.org/ns/activitystreams#OrderedCollection",
	"CollectionPage":        "www.w3.org/ns/activitystreams#CollectionPage",
	"OrderedCollectionPage": "www.w3.org/ns/activitystreams#OrderedCollectionPage",
	"Accept":                "www.w3.org/ns/activitystreams#Accept",
	"TentativeAccept":       "www.w3.org/ns/activitystreams#TentativeAccept",
	"Add":                   "www.w3.org/ns/activitystreams#Add",
	"Arrive":                "www.w3.org/ns/activitystreams#Arrive",
	"Create":                "www.w3.org/ns/activitystreams#Create",
	"Delete":                "www.w3.org/ns/activitystreams#Delete",
	"Follow":                "www.w3.org/ns/activitystreams#Follow",
	"Ignore":                "www.w3.org/ns/activitystreams#Ignore",
	"Join":                  "www.w3.org/ns/activitystreams#Join",
	"Leave":                 "www.w3.org/ns/activitystreams#Leave",
	"Like":                  "www.w3.org/ns/activitystreams#Like",
	"Offer":                 "www.w3.org/ns/activitystreams#Offer",
	"Invite":                "www.w3.org/ns/activitystreams#Invite",
	"Reject":                "www.w3.org/ns/activitystreams#Reject",
	"TentativeReject":       "www.w3.org/ns/activitystreams#TentativeReject",
	"Remove":                "www.w3.org/ns/activitystreams#Remove",
	"Undo":                  "www.w3.org/ns/activitystreams#Undo",
	"Update":                "www.w3.org/ns/activitystreams#Update",
	"View":                  "www.w3.org/ns/activitystreams#View",
	"Listen":                "www.w3.org/ns/activitystreams#Listen",
	"Read":                  "www.w3.org/ns/activitystreams#Read",
	"Move":                  "www.w3.org/ns/activitystreams#Move",
	"Travel":                "www.w3.org/ns/activitystreams#Travel",
	"Announce":              "www.w3.org/ns/activitystreams#Announce",
	"Block":                 "www.w3.org/ns/activitystreams#Block",
	"Flag":                  "www.w3.org/ns/activitystreams#Flag",
	"Dislike":               "www.w3.org/ns/activitystreams#Dislike",
	"Question":              "www.w3.org/ns/activitystreams#Question",
	"Application":           "www.w3.org/ns/activitystreams#Application",
	"Group":                 "www.w3.org/ns/activitystreams#Group",
	"Organization":          "www.w3.org/ns/activitystreams#Organization",
	"Person":                "www.w3.org/ns/activitystreams#Person",
	"Service":               "www.w3.org/ns/activitystreams#Service",
	"Relationship":          "www.w3.org/ns/activitystreams#Relationship",
	"Article":               "www.w3.org/ns/activitystreams#Article",
	"Document":              "www.w3.org/ns/activitystreams#Document",
	"Audio":                 "www.w3.org/ns/activitystreams#Audio",
	"Image":                 "www.w3.org/ns/activitystreams#Image",
	"Video":                 "www.w3.org/ns/activitystreams#Video",
	"Note":                  "www.w3.org/ns/activitystreams#Note",
	"Page":                  "www.w3.org/ns/activitystreams#Page",
	"Event":                 "www.w3.org/ns/activitystreams#Event",
	"Place":                 "www.w3.org/ns/activitystreams#Place",
	"Mention":               "www.w3.org/ns/activitystreams#Mention",
	"Profile":               "www.w3.org/ns/activitystreams#Profile",
	"Tombstone":             "www.w3.org/ns/activitystreams#Tombstone",
}

type TypeNode struct {
	Multi bool // Can this field be a list?
	Types []string
}

// Maps built in field IRIs to their type descriptor
var BuiltinFieldTypes = map[string]TypeNode{
	"@id":                                 TypeNode{false, []string{"iri"}},
	"@type":                               TypeNode{true, []string{"iri"}}, // Not actually handled here
	"www.w3.org/ns/activitystreams#actor": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#attachment": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#attributedTo": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#audience": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#bcc": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#bto": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#cc":  TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#context": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#current": TypeNode{false,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#first": TypeNode{false, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#generator": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#icon":  TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#image": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#inReplyTo": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#instrument": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#last": TypeNode{false, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#location": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#items": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#oneOf": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#anyOf": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#closed": TypeNode{false,
		[]string{"bool", "datetime", "iri/obj"}},
	"www.w3.org/ns/activitystreams#origin": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#next":   TypeNode{false, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#object": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#prev":   TypeNode{false, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#preview": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#result": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#replies": TypeNode{false,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#tag":    TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#target": TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#to":     TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#url":    TypeNode{true, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#accuracy": TypeNode{false,
		[]string{"float"}},
	"www.w3.org/ns/activitystreams#altitude": TypeNode{false,
		[]string{"float"}},
	"www.w3.org/ns/activitystreams#content": TypeNode{false,
		[]string{"string", "langMap"}},
	"www.w3.org/ns/activitystreams#contentMap": TypeNode{false,
		[]string{"langMap", "string"}},
	"www.w3.org/ns/activitystreams#name": TypeNode{false,
		[]string{"string", "langMap"}},
	"www.w3.org/ns/activitystreams#nameMap": TypeNode{false,
		[]string{"langMap", "string"}},
	"www.w3.org/ns/activitystreams#duration": TypeNode{false,
		[]string{"duration"}},
	"www.w3.org/ns/activitystreams#height": TypeNode{false, []string{"uint"}},
	"www.w3.org/ns/activitystreams#href":   TypeNode{false, []string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#hreflang": TypeNode{false,
		[]string{"string"}}, // TODO: lang tags?
	"www.w3.org/ns/activitystreams#partOf": TypeNode{false,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#latitude": TypeNode{false,
		[]string{"float"}},
	"www.w3.org/ns/activitystreams#longitude": TypeNode{false,
		[]string{"float"}},
	"www.w3.org/ns/activitystreams#mediaType": TypeNode{false,
		[]string{"string"}},
	"www.w3.org/ns/activitystreams#endTime": TypeNode{false,
		[]string{"datetime"}},
	"www.w3.org/ns/activitystreams#published": TypeNode{false,
		[]string{"datetime"}},
	"www.w3.org/ns/activitystreams#startTime": TypeNode{false,
		[]string{"datetime"}},
	"www.w3.org/ns/activitystreams#radius": TypeNode{false, []string{"float"}},
	"www.w3.org/ns/activitystreams#rel":    TypeNode{true, []string{"string"}},
	"www.w3.org/ns/activitystreams#startIndex": TypeNode{false,
		[]string{"uint"}},
	"www.w3.org/ns/activitystreams#summary": TypeNode{false,
		[]string{"string", "langMap"}},
	"www.w3.org/ns/activitystreams#summaryMap": TypeNode{false,
		[]string{"langMap", "string"}},
	"www.w3.org/ns/activitystreams#totalItems": TypeNode{false,
		[]string{"uint"}},
	"www.w3.org/ns/activitystreams#units": TypeNode{false,
		[]string{"iri", "string"}},
	"www.w3.org/ns/activitystreams#updated": TypeNode{false,
		[]string{"datetime"}},
	"www.w3.org/ns/activitystreams#width": TypeNode{false, []string{"width"}},
	"www.w3.org/ns/activitystreams#subject": TypeNode{false,
		[]string{"subject"}},
	"www.w3.org/ns/activitystreams#relationship": TypeNode{true,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#describes": TypeNode{false,
		[]string{"iri/obj"}},
	"www.w3.org/ns/activitystreams#formerType": TypeNode{true,
		[]string{"iri/obj"}}, // TODO: type / iri
	"www.w3.org/ns/activitystreams#deleted": TypeNode{false,
		[]string{"datetime"}},
}

func ClipIRI(iri string) (clippedIRI string) {
	// if has "://" clip off that and everything before
	index := strings.Index(iri, "://")
	if index == -1 {
		return iri
	} else {
		return iri[index+3:]
	}
}

// TODO: Compare IRI (http/https)

func BuiltinType(IRI string) (typeDescriptor *TypeNode) {
	temp, ok := BuiltinFieldTypes[IRI]
	typeDescriptor = &temp
	if ok {
		return typeDescriptor
	} else {
		return nil
	}
}

// Maps field IRIs to their type descriptor
var ExtensionFieldTypes = map[string]TypeNode{}

var TypeValidators = map[string]func(value interface{}) (valid bool){
	"float":    validateFloat,
	"datetime": validateDatetime,
	"duration": validateDuration, // no test
	"iri":      validateIRI,
	"string":   validateString,
	"bool":     validateBool,
	"iri/obj":  validateIRIObject,
	"obj":      validateSubObject,
	"complex":  validateComplexData,
	"int":      validateInt,  // no test
	"uint":     validateUInt, // no test
}

func validateComplexData(value interface{}) (valid bool) {
	// "Complex Data" are objects like the language map.
	// They can be identified by the lack of a @type
	switch typedV := value.(type) {
	case map[string]interface{}:
		_, hasType := typedV["@type"]
		if hasType == false {
			// No @type, try the alias
			_, hasType = typedV["type"]
		}
		// Valid only if no type exists
		return !hasType
	default:
		return false
	}
}

func validateSubObject(value interface{}) (valid bool) {
	// Types: ID + extra data, transient, embedded (full)
	// check for @type/type, if none then complex data not an object
	// else one of embedded, ID+, or transient
	switch typedV := value.(type) {
	case map[string]interface{}:
		_, hasType := typedV["@type"]
		if hasType == false {
			// No @type, try the alias
			_, hasType = typedV["type"]
		}
		// Valid only if the type exists
		return hasType
	default:
		return false
	}
}

func validateIRIObject(value interface{}) (valid bool) {
	valid = validateSubObject(value)
	if valid == false {
		valid = validateIRI(value)
	}
	return valid
}

func validateDatetime(value interface{}) (valid bool) {
	switch typedV := value.(type) {
	case string:
		// Try to parse as a datetime
		_, err := time.Parse(time.RFC3339Nano, typedV)
		if err != nil {
			return false
		} else {
			return true
		}
	default:
		return false
	}
}

func validateDuration(value interface{}) (valid bool) {
	switch typedV := value.(type) {
	case string:
		// Try to parse as a duration
		_, err := duration.Parse(typedV)
		if err != nil {
			return false
		} else {
			return true
		}
	default:
		return false
	}
}

func validateIRI(value interface{}) (valid bool) {
	switch typedV := value.(type) {
	case string:
		// Try to parse as an IRI
		_, err := url.ParseRequestURI(typedV)
		if err != nil {
			return false
		} else {
			return true
		}
	default:
		return false
	}
}

func validateString(value interface{}) (valid bool) {
	switch value.(type) {
	case string:
		return true
	default:
		return false
	}
}

func validateFloat(value interface{}) (valid bool) {
	switch value.(type) {
	case float32:
		return true
	case float64:
		return true
	default:
		return false
	}
}

func validateInt(value interface{}) (valid bool) {
	switch value.(type) {
	case uint:
		return true
	case int:
		return true
	case uintptr:
		return true
	case uint8:
		return true
	case uint16:
		return true
	case uint32:
		return true
	case uint64:
		return true
	case int8:
		return true
	case int16:
		return true
	case int32:
		return true
	case int64:
		return true
	default:
		return false
	}
}

func validateUInt(value interface{}) (valid bool) {
	switch typedV := value.(type) {
	case uint:
		return true
	case int:
		return true
	case uintptr:
		return true
	case uint8:
		return true
	case uint16:
		return true
	case uint32:
		return true
	case uint64:
		return true
	case int8:
		if typedV >= 0 {
			return true
		}
	case int16:
		if typedV >= 0 {
			return true
		}
	case int32:
		if typedV >= 0 {
			return true
		}
	case int64:
		if typedV >= 0 {
			return true
		}
	default:
		return false
	}
	return false
}

func validateBool(value interface{}) (valid bool) {
	switch value.(type) {
	case bool:
		return true
	default:
		return false
	}
}
