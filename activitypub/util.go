package activitypub

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import ()

func mapifyString(field string, s *string, target *map[string]interface{}) {
	if *s != "" {
		(*target)[field] = *s
	}
}

func mapifyStringList(field string, lst *[]string, target *map[string]interface{}) {
	count := len(*lst)
	if count > 1 {
		newlst := make([]string, count)
		copy(newlst, *lst)
		(*target)[field] = newlst
	} else if count == 1 { // Single item lists are treated as scalars
		(*target)[field] = (*lst)[0]
	}
}

func mapifyLangMap(field string, m *map[string]string, target *map[string]interface{}) {
	if len(*m) > 0 {
		temp := make(map[string]string)
		for k, v := range *m {
			temp[k] = v
		}
		(*target)[field] = temp
	}
}

func handleLangMap(dest *map[string]string, source map[string]interface{}) (err error) {
	*dest = make(map[string]string)
	for k, v := range source {
		(*dest)[k] = v.(string)
	}
	return nil
}
