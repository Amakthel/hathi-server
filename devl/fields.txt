Copyright 2018-present Hathi authors, Apache 2.0

This document is a list of all known ActivityPub fields, as well as any we might add.

Multi: how many and what format of values are allowed:
    S   single
    L   list
    M   map
Name: name of the field
Value types: what kind of values are allowed
Class: what ActivityPub class these are defined within
Definied: what struct we define them in


Multi | Name         | Value types   | ActivityPub Class      | Defined
======+==============+===============+========================+===========
L     | Type         | string        | Object, Link           | Object
S     | Id           | string        | Object, Link           | Object
L     | AtContext    | string        | Object, Link           | Object
L     | Attachment   | reference     | Object, Link           | Object
L     | AttributedTo | reference     | Object, Link           | Object
L     | Audience     | reference     | Object, Link           | Object
M     | Content      | lang-map      | Object                 | Object
S     | Context      | string        | Object                 | Object
M     | Name         | lang-map      | Object, Link           | Object
S     | EndTime      | datetime      | Object                 | Object
S     | Generator    | reference     | Object                 | Object
L     | Icon         | reference     | Object                 | Object
L     | Image        | reference     | Object                 | Object
L     | InReplyTo    | reference     | Object                 | Object
L     | Location     | location      | Object                 | Object
S     | Preview      | reference     | Object, Link           | Object
S     | Published    | datetime      | Object                 | Object
S     | Replies      | reference     | Object                 | Object
S     | StartTime    | datetime      | Object                 | Object
L     | Tag          | reference     | Object                 | Object
S     | Updated      | datetime      | Object                 | Object
L     | Url          | ref, string   | Object                 | Object
L     | To           | reference     | Object                 | Object
L     | Bto          | reference     | Object, Link           | Object
L     | Cc           | reference     | Object, Link           | Object
L     | Bcc          | reference     | Object, Link           | Object
S     | MediaType    | MIME type     | Object, Link           | Object
S     | Duration     | duration      | Object                 | Object
S     | Altitude     | float         | Object                 | Object
M     | Summary      | lang-map      | Object                 | Object
S     | Origin       | reference     | Activity               | Activity
S     | Result       | reference [?] | Activity               | Activity
L     | Actor        | reference     | Activity               | Activity
L     | Instrument   | reference     | Activity               | Activity
L     | Object       | reference     | Activity, Relationship | Activity
L     | Target       | reference     | Activity               | Activity
L     | Items        | reference     | Collection             |
S     | Current      | reference     | Collection             |
S     | First        | ref, link     | Collection             |
S     | Last         | ref, link     | Collection             |
L     | OneOf        | reference     | Question               |
L     | AnyOf        | reference     | Question               |
S     | Closed       | datetime, ref | Question               |
S     | Next         | reference     | CollectionPage         |
S     | Prev         | reference     | CollectionPage         |
S     | Accuracy     | float         | Place                  |
S     | Height       | uint          | Link                   |
S     | Width        | uint          | Link                   |
S     | Href         | URL           | Link                   |
S     | HrefLang     | lang-code     | Link                   |
S     | PartOf       | reference     | CollectionPage         |
S     | Latitude     | float         | Place                  |
S     | Radius       | float         | Place                  |
L     | Rel          | link relation | Link                   |
S     | StartIndex   | uint          | OrderedCollectionPage  |
S     | TotalItems   | uint          | Collection             |
S     | Units        | unit          | Place                  |
S     | Subject      | reference     | Relationship           |
S     | Relationship | relationship  | Relationship           |
S     | Describes    | reference     | Profile                |
L     | FormerType   | type name     | Tombstone              | Object
S     | Deleted      | datetime      | Tombstone              | Object
