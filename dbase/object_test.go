package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	//"reflect"
	"testing"

	//"gitlab.com/hathi-social/hathi-server/activitypub"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

// ====== Store-ers ======

func TestStoreObjectData(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Set expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	itemRows := sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"})
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo").
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "@id", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "@id", 0, STRING, "foo", 0, 0.0, false, "")
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "fieldA", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "fieldA", 0, INTEGER, "", 42, 0.0, false, "")
	mock.ExpectCommit()
	// Run test
	err = db.StoreObjectData(nil, "foo",
		map[string]interface{}{"fieldA": 42, "@id": "foo"})
	if err != nil {
		t.Fatal("StoreObject faliure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreObject unfulfilled expectations:", err)
	}
}

func TestStoreField(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Single item
	// Set expectations
	mock.ExpectBegin()
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "field", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "field", 0, STRING, "bar", 0, 0.0, false, "")
	mock.ExpectCommit()
	// Run test
	err = db.StoreField(nil, "foo", "field", "bar")
	if err != nil {
		t.Fatal("StoreField faliure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreField unfulfilled expectations:", err)
	}

	// List
	// Set expectations
	mock.ExpectBegin()
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "field", 0)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "field", 0, STRING, "bar", 0, 0.0, false, "")
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "field", 1, INTEGER, "", 42, 0.0, false, "")
	mock.ExpectCommit()
	// Run test
	err = db.StoreField(nil, "foo", "field", []interface{}{"bar", 42})
	if err != nil {
		t.Fatal("StoreField faliure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreField unfulfilled expectations:", err)
	}
}

func TestStoreFieldItem(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Test normal item
	// Set expectations
	mock.ExpectBegin()
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "field", 42, STRING, "bar", 0, 0.0, false, "")
	mock.ExpectCommit()
	// Run test
	err = db.StoreFieldItem(nil, "foo", "field", 42, "bar")
	if err != nil {
		t.Fatal("StoreFieldItem faliure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreFieldItem unfulfilled expectations:", err)
	}

	// Test embedded object
	// Set expectations
	mock.ExpectBegin()
	// generating embed ID
	langRows := sqlmock.NewRows([]string{"LASTID"})
	mock.ExpectQuery("select LASTID from TRANSID").
		WillReturnRows(langRows)
	mock.ExpectExec("update TRANSID").
		WithArgs(1, "embed").
		WillReturnResult(sqlmock.NewResult(0, 0))
	// begin embedded
	mock.ExpectExec("delete from FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("^1")
	itemRows := sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"})
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("^1").
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("^1")
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("^1", "field", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("^1", "field", 0, STRING, "xyzzy", 0, 0.0, false, "")
	// end embedded
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "field", 42, EMBED, "", 0, 0.0, false, "^1")
	mock.ExpectCommit()
	// Run test
	err = db.StoreFieldItem(nil, "foo", "field", 42,
		map[string]interface{}{"field": "xyzzy"})
	if err != nil {
		t.Fatal("StoreFieldItem faliure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreFieldItem unfulfilled expectations:", err)
	}
}

func TestDeleteObjectData(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Set expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from FIELDS").
		WithArgs("foo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows := sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "fizzbin", 0, 0.0, false, "").
		AddRow(EMBED, "", 0, 0.0, false, "^F00DFEED")
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo").
		WillReturnRows(itemRows)
	// === start deleting subobject === //
	mock.ExpectExec("delete from FIELDS").
		WithArgs("^F00DFEED").
		WillReturnResult(sqlmock.NewResult(0, 0))
	// getting sub-object rows
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "quux", 0, 0.0, false, "")
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("^F00DFEED").
		WillReturnRows(itemRows)
	// delete sub-object row
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("^F00DFEED").
		WillReturnResult(sqlmock.NewResult(0, 0))
	// === end deleting subobject === //
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("foo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()
	// Run test
	err = db.DeleteObjectData(nil, "foo")
	if err != nil {
		t.Fatal("DeleteObjectData failure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("DeleteObjectData unfulfilled expectations:", err)
	}
}

// ====== Loaders ======

func TestLoadObjectData(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Set expectations
	mock.ExpectBegin()
	objRows := sqlmock.NewRows([]string{"NAME", "SINGLE"}).
		AddRow("fld", 0)
	mock.ExpectQuery("select NAME, SINGLE from FIELDS").
		WithArgs("foo").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "quux", 0, 0.0, false, "").
		AddRow(INTEGER, "", 23, 0.0, false, "")
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "fld").
		WillReturnRows(objRows)
	mock.ExpectCommit()
	// Run test
	data, err := db.LoadObjectData(nil, "foo")
	if err != nil {
		t.Fatal("LoadObjectData failure:", err)
	}
	if (len(data["fld"].([]interface{})) != 2) ||
		(data["fld"].([]interface{})[0] != "quux") ||
		(data["fld"].([]interface{})[1].(int64) != 23) {
		t.Fatal("LoadObjectData failure:", data)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("LoadObjectData unfulfilled expectations:", err)
	}
}

func TestLoadFieldItems(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Set expectations
	mock.ExpectBegin()
	objRows := sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "quux", 0, 0.0, false, "").
		AddRow(INTEGER, "", 23, 0.0, false, "").
		AddRow(EMBED, "", 0, 0.0, false, "^0")
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "fld").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"NAME", "SINGLE"}).
		AddRow("subfield", 1)
	mock.ExpectQuery("select NAME, SINGLE from FIELDS").
		WithArgs("^0").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "sub", 0, 0.0, false, "")
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("^0", "subfield").
		WillReturnRows(objRows)
	mock.ExpectCommit()
	// Run test
	data, err := db.LoadFieldItems(nil, "foo", "fld")
	if err != nil {
		t.Fatal("LoadFieldItems failure:", err)
	}
	if (len(data) != 3) || (data[0] != "quux") || (data[1].(int64) != 23) ||
		data[2].(map[string]interface{})["subfield"] != "sub" {
		t.Fatal("LoadFieldItems failure:", data)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("LoadFieldItems unfulfilled expectations:", err)
	}
}
