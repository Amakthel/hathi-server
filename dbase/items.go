package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	"database/sql"
	//"gitlab.com/hathi-social/hathi-server/activitypub"
)

// List handling functions
// Because of mailboxes (among other things) we often need to manipulate
// lists in ways that do not involve changing any other details of the object.
// It would be wasteful to load, modify, delete, and store an entire object.
//
// What we have here:
// GetI: Get item at index I
// SetI: Set item at index I
// GetRange: get items within a range (think pagination)
// AddI: Add item at index I, moving existing indices up if necessary
// RmI: Remove item from index I, moving existing indices down in necessary
// Append: equivilent to an AddI at the top of the list
// EditI: change value at index I

func (h *HathiDB) ListGetI(tx *sql.Tx, objID string, fieldName string, index int) (value interface{}, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	query := "select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA where ID = $1 and NAME = $2 and ITEMINDEX = $3"
	items, err := h.DB.Query(query, objID, fieldName, index)
	if err != nil {
		return nil, err
	}
	defer items.Close()
	if items.Next() != true {
		return nil, items.Err()
	}
	value, vType, err := h.loadValueAndEmbeds(tx, items)
	if vType == EMBED {
		// Retreive sub-object
		value, err = h.LoadObjectData(tx, value.(string))
		if err != nil {
			return nil, err
		}
	}
	if err != nil {
		return nil, err
	}
	return value, nil
}

func (h *HathiDB) ListGetRange(tx *sql.Tx, objID string, fieldName string, start int, length int) (values []interface{}, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	var data *sql.Rows
	if length > 0 { // Bounded range, upwards
		query := "select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA where ID = $1 and NAME = $2 and ITEMINDEX >= $3 and ITEMINDEX < $4 order by INDEX asc"
		data, err = tx.Query(query, objID, fieldName, start, start+length)
	} else if length < 0 { // Bounded range, downwards
		query := "select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA where ID = $1 and NAME = $2 and ITEMINDEX >= $3 and ITEMINDEX < $4 order by INDEX asc"
		data, err = tx.Query(query, objID, fieldName, start+length, start+1)
	} else { // Unbounded range
		query := "select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA where ID = $1 and NAME = $2 and ITEMINDEX >= $3 order by ITEMINDEX asc"
		data, err = tx.Query(query, objID, fieldName, start)
	}
	if err != nil {
		return nil, err
	}
	defer data.Close()
	values = []interface{}{}
	for data.Next() {
		temp, _, err := h.loadValueAndEmbeds(tx, data)
		if err != nil {
			return nil, err
		}
		values = append(values, temp)
	}
	return values, nil
}

func (h *HathiDB) ListAppend(tx *sql.Tx, objID string, fieldName string, value interface{}) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	err = h.checkAndCreateFields(tx, objID, fieldName, false)
	if err != nil {
		return err
	}
	index, err := h.ListMaxIndex(tx, objID, fieldName)
	if err != nil {
		return err
	}
	index++
	err = h.StoreFieldItem(tx, objID, fieldName, index, value)
	if err != nil {
		return err
	}
	return nil
}

func (h *HathiDB) ListAddI(tx *sql.Tx, objID string, fieldName string, index int, value interface{}) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	err = h.checkAndCreateFields(tx, objID, fieldName, false)
	if err != nil {
		return err
	}
	// Check that row doesn't exist
	testValue, err := h.ListGetI(tx, objID, fieldName, index)
	if err != nil {
		return err
	}
	if testValue != nil { // There is something at the index
		// Move everything out of the way
		err = h.shiftEntries(tx, objID, fieldName, index, 1)
		if err != nil {
			return err
		}
	}
	// Add row
	err = h.StoreFieldItem(tx, objID, fieldName, index, value)
	if err != nil {
		return err
	}
	return nil
}

func (h *HathiDB) ListRM(tx *sql.Tx, objID string, fieldName string, index int) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Get item I, if it exists check for embed
	query := "select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA where ID = $1 and NAME = $2 and ITEMINDEX = $3;"
	data, err := tx.Query(query, objID, fieldName, index)
	if err != nil {
		return err
	}
	defer data.Close()
	if data.Next() != true {
		return data.Err()
	}
	value, vType, err := loadValue(data)
	if err != nil {
		return err
	}
	if vType == EMBED {
		// check and delete transient objects
		if value.(string)[0] == '^' { // transient object ID
			err = h.DeleteObjectData(tx, value.(string))
			if err != nil {
				return err
			}
		}
	}
	// Delete item
	query = "delete from FIELDDATA where ID = $1 and NAME = $2 and ITEMINDEX = $3"
	_, err = tx.Exec(query, objID, fieldName, index)
	if err != nil {
		return err
	}
	// Move everything down
	err = h.shiftEntries(tx, objID, fieldName, index+1, -1)
	if err != nil {
		return err
	}
	return nil
}

func (h *HathiDB) ListMaxIndex(tx *sql.Tx, objID string, fieldName string) (index int, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return 0, err
		}
		defer h.EndTransaction(tx, &err)
	}
	query := "select max(ITEMINDEX) as MAXID from FIELDDATA where ID = $1 and NAME = $2"
	// Get highest index
	rows, err := h.DB.Query(query, objID, fieldName)
	if err != nil {
		return 0, err
	}
	defer rows.Close()
	if rows.Next() != true { // Nothing in DB, start at 0
		index = -1
	} else {
		rows.Scan(&index)
	}
	return index, nil
}

func (h *HathiDB) ListSetI(tx *sql.Tx, objID string, fieldName string, index int, value interface{}) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	err = h.checkAndCreateFields(tx, objID, fieldName, false)
	if err != nil {
		return err
	}
	err = h.ListRM(tx, objID, fieldName, index)
	if err != nil {
		return err
	}
	err = h.StoreFieldItem(tx, objID, fieldName, index, value)
	if err != nil {
		return err
	}
	return nil
}

// Checks that FIELDS row exists for an object field, returns the single value.
func (h *HathiDB) checkAndCreateFields(tx *sql.Tx, objID string, fieldName string, desiredSingle bool) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// load FIELDS row for objID and fieldName
	fields, err := tx.Query(
		"select SINGLE from FIELDS where ID = $1 and NAME = $2",
		objID, fieldName,
	)
	if err != nil {
		return err
	}
	defer fields.Close()
	if fields.Next() != true {
		// No FIELDS row, we must make one.
		_, err = tx.Exec(
			"insert into FIELDS (ID, NAME, SINGLE) values ($1, $2, $3)",
			objID, fieldName, desiredSingle,
		)
		if err != nil {
			return err
		}
		// We are done here.
		return nil
	}
	// Row exists, check that single == desiredSingle
	var single bool
	err = fields.Scan(&single)
	if err != nil {
		return err
	}
	if single != desiredSingle {
		// Update the existing row
		_, err := tx.Exec(
			"update FIELDS set SINGLE = $1 where ID = $2 and NAME = $3",
			desiredSingle, objID, fieldName,
		)
		if err != nil {
			return err
		}
	}
	return nil
}
