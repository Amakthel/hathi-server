package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	//"reflect"
	"errors"
	"testing"

	//"gitlab.com/hathi-social/hathi-server/activitypub"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestDatabaseCOC(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Test the creation of each table
	mock.ExpectBegin()
	mock.ExpectExec(`create table FIELDS`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table FIELDDATA`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table TRANSID`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table PRIVILEGE`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	db.Initialize()

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database Initialize: unfulfilled expectations:", err)
	}

	// Test closing
	mock.ExpectClose()

	db.Close()

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database Close: unfulfilled expectations:", err)
	}
}

func TestBeginTransaction(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	mock.ExpectBegin()

	tx, err := db.BeginTransaction()

	if tx == nil || err != nil {
		t.Fatal("Database BeginTransaction error:", tx, err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database BeginTransaction: unfulfilled expectations:", err)
	}
}

func TestEndTransaction(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Test without error control
	mock.ExpectBegin()
	mock.ExpectCommit()
	tx, err := db.BeginTransaction()
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	err = db.EndTransaction(tx, nil)
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database EndTransaction: unfulfilled expectations:", err)
	}

	// Test with error control, success
	mock.ExpectBegin()
	mock.ExpectCommit()
	tx, err = db.BeginTransaction()
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	testError := error(nil)
	err = db.EndTransaction(tx, &testError)
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database EndTransaction: unfulfilled expectations:", err)
	}

	// Test with error control, rollback
	mock.ExpectBegin()
	mock.ExpectRollback()
	tx, err = db.BeginTransaction()
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	testError = errors.New("foo")
	err = db.EndTransaction(tx, &testError)
	if err != nil {
		t.Fatal("Database EndTransaction error:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database EndTransaction: unfulfilled expectations:", err)
	}
}
