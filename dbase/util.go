package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"database/sql"
	//"sort"
	"errors"
	"fmt"
	"gitlab.com/hathi-social/hathi-server/activitypub"
)

// Type codes
var STRING = 1
var INTEGER = 2
var FLOAT = 3
var BOOL = 4
var EMBED = 5
var NULL = 6

type boxEntry struct {
	CollectionID string
	EntryID      string
	Index        int
}

func (b *boxEntry) isEqual(other *boxEntry) bool {
	if b.Index != other.Index {
		return false
	} else if b.CollectionID != other.CollectionID {
		return false
	} else if b.EntryID != other.EntryID {
		return false
	}
	return true
}

// The caller is responsible for closing the rows
func convertBoxRowsToSlice(rows *sql.Rows) (indices []int, err error) {
	for rows.Next() {
		var index int
		err = rows.Scan(&index)
		if err != nil {
			return nil, err
		}
		indices = append(indices, index)
	}
	return indices, nil
}

// TODO: Remove or change to field base
// Move everything from top through snipIndex up or down
func (h *HathiDB) shiftEntries(tx *sql.Tx, objID string, fieldName string, snipIndex int, movement int) (err error) {
	setExec := "update FIELDDATA set ITEMINDEX = $1 where ID = $2 and NAME = $3 and ITEMINDEX = $4"
	queryRoot := "select ITEMINDEX from FIELDDATA where ID = $1 and NAME = $2 and INDEX >= $3 order by ITEMINDEX "
	var data *sql.Rows
	// Get the block of rows that we are modifying
	if movement > 0 { // Move up
		// Get list
		data, err = tx.Query(queryRoot+"desc", objID, fieldName, snipIndex)
		if err != nil {
			return err
		}
	} else if movement < 0 { // Move down
		data, err = tx.Query(queryRoot+"asc", objID, fieldName, snipIndex)
		if err != nil {
			return err
		}
	} else { // movement == 0
		return nil // NOP, should this be an error?
	}
	defer data.Close()
	indices, err := convertBoxRowsToSlice(data)
	if err != nil {
		return err
	}
	// Have the rows in a convienent format, now move them around
	for _, index := range indices {
		newIndex := index + movement
		_, err = tx.Exec(setExec, newIndex, objID, fieldName, index)
		if err != nil { // Problem: the db is in an unknown state here
			return err
		}
	}
	return nil
}

func (h *HathiDB) getAllTableIDs(tx *sql.Tx, tableName string) (ids []string, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	objTable, err := tx.Query(`select ID from ` + tableName)
	defer objTable.Close()
	ids = make([]string, 0)
	var entry string
	for objTable.Next() {
		err = objTable.Scan(&entry)
		if err != nil {
			return nil, err
		}
		ids = append(ids, entry)
	}
	return ids, nil
}

// Converts a value from an object into a type code and the set of variables
// needed by the datebase
func ValueToTypes(value interface{}) (typeCode int, vStr string, vInt int64, vFloat float64, vBool bool, vEmbed string, err error) {
	vStr = ""
	vInt = 0
	vFloat = 0.0
	vBool = false
	vEmbed = ""
	if value == nil { // NULL type
		return NULL, "", 0, 0.0, false, "", nil
	}
	switch typedV := value.(type) {
	case string:
		typeCode = STRING
		vStr = typedV
	case int:
		typeCode = INTEGER
		vInt = int64(typedV)
	case int8:
		typeCode = INTEGER
		vInt = int64(typedV)
	case int16:
		typeCode = INTEGER
		vInt = int64(typedV)
	case int32:
		typeCode = INTEGER
		vInt = int64(typedV)
	case int64:
		typeCode = INTEGER
		vInt = typedV
	case uint:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uint8:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uint16:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uint32:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uint64:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uintptr:
		typeCode = INTEGER
		vInt = int64(typedV)
	case float32:
		typeCode = FLOAT
		vFloat = float64(typedV)
	case float64:
		typeCode = FLOAT
		vFloat = typedV
	case bool:
		typeCode = BOOL
		vBool = typedV
	case map[string]interface{}:
		// Proper embed handling will have the caller extracting the data.
		typeCode = EMBED
		// If the object has an ID we return that, otherwise the caller
		// will need to create an internal ID
		var temp interface{}
		var ok bool
		temp, ok = typedV["@id"]
		if !ok { // Try the alias
			temp, ok = typedV["id"]
		}
		// Make sure it is a string
		switch typedID := temp.(type) {
		case string:
			vEmbed = typedID
		}
	case *activitypub.Object:
		// Proper embed handling will have the caller extracting the data.
		typeCode = EMBED
		// If the object has an ID we return that, otherwise the caller
		// will need to create an internal ID
		objID := typedV.GetID()
		// Make sure it is a string
		vEmbed = objID
	default:
		return 0, "", 0, 0.0, false, "", errors.New("Invalid type")
	}
	return typeCode, vStr, vInt, vFloat, vBool, vEmbed, nil
}

func TypesToValue(typeCode int, vStr string, vInt int64, vFloat float64, vBool bool, vEmbed string) (value interface{}, err error) {
	switch typeCode {
	case STRING:
		value = vStr
	case INTEGER:
		value = vInt
	case FLOAT:
		value = vFloat
	case BOOL:
		value = vBool
	case EMBED:
		value = vEmbed
	case NULL:
		value = nil
	default:
		return nil, errors.New("Invalid type code")
	}
	return value, nil
}

func ListToInterfaceList(value interface{}) (result []interface{}, err error) {
	switch typedV := value.(type) {
	case []string:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int8:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int16:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int32:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int64:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint8:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint16:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint32:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint64:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uintptr:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []float32:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []float64:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []bool:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []map[string]interface{}:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []*activitypub.Object:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []interface{}:
		result = typedV
	default:
		return nil, errors.New("Invalid type")
	}
	return result, nil
}

func (h *HathiDB) GenerateIDCode(tx *sql.Tx, idName string) (id string, err error) {
	// Generates the next ID in the set idName
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return "", err
		}
		defer h.EndTransaction(tx, &err)
	}
	// load last id
	objTable, err := tx.Query("select LASTID from TRANSID where NAME = $1",
		idName)
	if err != nil {
		return "", err
	}
	defer objTable.Close()
	var lastIDCode int64
	if objTable.Next() != true {
		// Found nothing, start at zero
		lastIDCode = 0
	} else {
		// Found something
		err = objTable.Scan(&lastIDCode)
		if err != nil {
			return "", err
		}
	}
	// calc new id
	newIDCode := lastIDCode + 1
	// store new id as last id
	_, err = tx.Exec("update TRANSID set LASTID = $1 where NAME = $2",
		newIDCode, idName)
	if err != nil {
		return "", err
	}
	// stringify and return
	id = fmt.Sprintf("%X", newIDCode)
	return id, nil
}

func loadValue(rows *sql.Rows) (value interface{}, vType int, err error) {
	var vString, vEmbed string
	var vInteger int64
	var vBoolean bool
	var vFloat float64
	err = rows.Scan(&vType, &vString, &vInteger, &vFloat, &vBoolean, &vEmbed)
	if err != nil {
		return nil, 0, err
	}
	value, err = TypesToValue(vType, vString, vInteger, vFloat, vBoolean,
		vEmbed)
	if err != nil {
		return nil, 0, err
	}
	return value, vType, nil
}

func (h *HathiDB) loadValueAndEmbeds(tx *sql.Tx, rows *sql.Rows) (value interface{}, vType int, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return "", 0, err
		}
		defer h.EndTransaction(tx, &err)
	}
	value, vType, err = loadValue(rows)
	if err != nil {
		return nil, 0, err
	}
	if vType == EMBED {
		value, err = h.LoadObjectData(tx, value.(string))
		if err != nil {
			return nil, 0, err
		}
	}
	return value, vType, nil
}
