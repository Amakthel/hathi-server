package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	"reflect"
	"testing"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	//"gitlab.com/hathi-social/hathi-server/activitypub"
)

func TestPrivileges(t *testing.T) {
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// ===================================
	// SetObjectPrivileges
	// ===================================
	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectExec("insert into PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Atlas", "parasite")
	mock.ExpectExec("insert into PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Atlas", "ambitious")
	mock.ExpectExec("insert into PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Delta", "awake")
	mock.ExpectExec("insert into PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Lamb", "parasite")
	mock.ExpectCommit()
	// Do test
	privs := map[string][]string{
		"Atlas": []string{"parasite", "ambitious"},
		"Lamb":  []string{"parasite"},
		"Delta": []string{"awake"},
	}
	err = db.SetObjectPrivileges(nil, "foo", privs)
	if err != nil {
		t.Fatal("Database PrivilegeSetAll:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database PrivilegeSetAll: unfulfilled expectations:", err)
	}
	// ===================================
	// GetObjectPrivileges
	// ===================================
	// Set up expectations
	mock.ExpectBegin()
	objRows := sqlmock.NewRows([]string{"ACTOR", "BITNAME"}).
		AddRow("Atlas", "parasite").
		AddRow("Atlas", "ambitious").
		AddRow("Lamb", "parasite").
		AddRow("Delta", "awake")
	mock.ExpectQuery("select ACTOR, BITNAME from PRIVILEGE").
		WithArgs("foo").
		WillReturnRows(objRows)
	mock.ExpectCommit()
	// Run test
	data, err := db.GetObjectPrivileges(nil, "foo")
	if err != nil {
		t.Fatal("Database PrivilegeGetAll:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database PrivilegeGetAll: unfulfilled expectations:", err)
	}
	privsExpected := map[string][]string{
		"Atlas": []string{"parasite", "ambitious"},
		"Lamb":  []string{"parasite"},
		"Delta": []string{"awake"},
	}
	if reflect.DeepEqual(data, privsExpected) != true {
		t.Fatal("Database PrivilegeGetAll: equality failure:", data)
	}
	// ===================================
	// SetObjActPrivileges
	// ===================================
	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Atlas")
	mock.ExpectExec("insert into PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Atlas", "parasite")
	mock.ExpectExec("insert into PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Atlas", "ambitious")
	mock.ExpectCommit()
	// Do test
	err = db.SetObjActPrivileges(nil, "foo", "Atlas",
		[]string{"parasite", "ambitious"})
	if err != nil {
		t.Fatal("Database PrivilegeSetActor:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database PrivilegeSetActor: unfulfilled expectations:", err)
	}
	// ===================================
	// GetObjActPrivileges
	// ===================================
	// Set up expectations
	mock.ExpectBegin()
	objRows = sqlmock.NewRows([]string{"BITNAME"}).
		AddRow("parasite").
		AddRow("ambitious")
	mock.ExpectQuery("select BITNAME from PRIVILEGE").
		WithArgs("foo", "Atlas").
		WillReturnRows(objRows)
	mock.ExpectCommit()
	// Run test
	bitlist, err := db.GetObjActPrivileges(nil, "foo", "Atlas")
	if err != nil {
		t.Fatal("Database PrivilegeGetActor:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database PrivilegeGetActor: unfulfilled expectations:", err)
	}
	bitsExpected := []string{"parasite", "ambitious"}
	if reflect.DeepEqual(bitlist, bitsExpected) != true {
		t.Fatal("Database PrivilegeGetActor: equality failure:", bitlist)
	}
	// ===================================
	// PrivilegeCheck
	// ===================================
	// Test exists
	// Set up expectations
	mock.ExpectBegin()
	objRows = sqlmock.NewRows([]string{"ID", "ACTOR", "BITNAME"}).
		AddRow("foo", "Atlas", "parasite")
	mock.ExpectQuery("select \\* from PRIVILEGE").
		WithArgs("foo", "Atlas", "parasite").
		WillReturnRows(objRows)
	mock.ExpectCommit()
	// Run test
	bit, err := db.CheckPrivilege(nil, "foo", "Atlas", "parasite")
	if err != nil {
		t.Fatal("Database PrivilegeCheck:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database PrivilegeCheck: unfulfilled expectations:", err)
	}
	if bit != true {
		t.Fatal("Database PrivilegeCheck: bool failure:", bit)
	}
	// Test doesn't exist
	// Set up expectations
	mock.ExpectBegin()
	objRows = sqlmock.NewRows([]string{"ID", "ACTOR", "BITNAME"})
	mock.ExpectQuery("select \\* from PRIVILEGE").
		WithArgs("foo", "Atlas", "decency").
		WillReturnRows(objRows)
	mock.ExpectCommit()
	// Run test
	bit, err = db.CheckPrivilege(nil, "foo", "Atlas", "decency")
	if err != nil {
		t.Fatal("Database PrivilegeCheck:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database PrivilegeCheck: unfulfilled expectations:", err)
	}
	if bit != false {
		t.Fatal("Database PrivilegeCheck: bool failure:", bit)
	}
	// ===================================
	// PrivilegeSet true
	// ===================================
	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Atlas", "corpse")
	mock.ExpectExec("insert into PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Atlas", "corpse")
	mock.ExpectCommit()
	// Run test
	err = db.SetPrivilege(nil, "foo", "Atlas", "corpse", true)
	if err != nil {
		t.Fatal("Database PrivilegeSet:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database PrivilegeSet: unfulfilled expectations:", err)
	}
	// ===================================
	// PrivilegeSet false
	// ===================================
	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from PRIVILEGE").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "Atlas", "life")
	mock.ExpectCommit()
	// Run test
	err = db.SetPrivilege(nil, "foo", "Atlas", "life", false)
	if err != nil {
		t.Fatal("Database PrivilegeClear:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database PrivilegeClear: unfulfilled expectations:", err)
	}
}
