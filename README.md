*2019-03-01:*  
*We, the Hathi authors,  are continuing this project and claim copyright to the changes that we will be making henceforth.*  
*We will be continuing to publish under the Apache open source license.*  
*2019-02-28:*  
*As our sponsoring organisation has decided to spin down, it has been agreed to public-domain the Hathi project.*  
*Code created on or before this date is free for everyone to use and/or claim.*  

# Hathi Server

## Developing

**Dependencies**

duration
go-sqlite3
go-sqlmock.v1
hathi-protocol

The build scripts use golang's ability to fetch dependencies as needed.

**1. Install the Go language**
Follow the official instructions at [GoLang](https://golang.org/doc/install) or
```
sudo apt-get install golang
```

Hathi requires golang version 1.11 minimum, earlier versions of Ubuntu can use
this backport PPA:

sudo add-apt-repository ppa:longsleep/golang-backports
sudo apt-get update
sudo apt-get install golang-go

**2. Clone from repository**
```
git clone gitlab.com/hathi-social/hathi-server
```

**3. Build server**
```
make build
```

The repo should *not* be within GOPATH. This project makes use of Go's new
module system, which does not work properly when placed under GOPATH.

**4. Run Server**
```
./server -port <port> -host <host>
```
Port and Host are optional, if ommited the server will default to running on
localhost:8080

If you run the server and everything is going according to plan, you should
see a line that lists the date and time, followed by "Starting server..."

## Testing

```
make test
```

## Using with client-mocker

The client-mocker isn't part of this project, it's part of 
hathi-social/hathi-protocol, but it's important that the client-mocker play
nicely with the server, so that we can make sure that the server actually
works.

Build the server as listed above.

Build the client-mocker as listed in the hathi-social/hathi-protocol README.md.

Run the server with 

```
./server
```
and leave it running.

Run the client-mocker with one of the test files in /test as a parameter. This
should look like

```
./client-mocker /path/to/project/hathi-server/tests/name-of-test-data.hathitest
```

You should see several lines similar to the following:

```
Running sequence: /path/to/wherever/you/keep/the/project/hathi-server/tests/name-of-test-data.hathitest
name-of-test-data.hathitest... 1/3
name-of-test-data.hathitest... 2/3
name-of-test-data.hathitest... 3/3
All sequences successful.
```

If you do, the test passed successfully. If you don't, something went wrong.

Each time you test the server, you must shut down the server, and delete
hathi.db. It should be in the same directory that you are currenty running 
the server. If you fail to do this before testing the server again, you'll
see something like the following:

```
Running sequence: /path/to/wherever/you/keep/the/project/hathi-server/tests/name-of-test-data.hathitest
name-of-test-data.hathitest... 1/3
Value unequal /actor-create-test actor
ERROR, packet doesn't match: mismatched actor: "/actor-create-test", ""
```

That's one error I got when I failed to delete the database between testing
runs. If you get an error on the very first test, then check to make sure
that you've shut down the server, and deleted the database. Then try again.

## Dumping / Restoring the database to a file

hathi-server has the -dump <file> option, which exports the database in JSON
format to the specified file and exits. Paired with the -restore <file> option
allows the server admin to update their version of hathi-server without
potential database incompatabilities.

NOTE: This system has not been updated yet after some major refactors, and
probably does not work as a result.
